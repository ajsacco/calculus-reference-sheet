\documentclass{report}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{minted}
\usemintedstyle{vs}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{setspace}
\usepackage{multicol}

\usepackage{graphicx}
\graphicspath{ {./images/} }

\doublespacing

\usepackage{amsmath}
\DeclareMathOperator{\arcsec}{arcsec}
\DeclareMathOperator{\arccot}{arccot}
\DeclareMathOperator{\arccsc}{arccsc}


%New colors defined below
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\usepackage[letterpaper, portrait, margin=.5in]{geometry}

\title{Calculus Reference Sheet}
\author{}
\date{May 2020}

%Code listing style named "mystyle"
\lstdefinestyle{mystyle}{
  backgroundcolor=\color{backcolour},   commentstyle=\color{codegreen},
  keywordstyle=\color{magenta},
  numberstyle=\tiny\color{codegray},
  stringstyle=\color{codepurple},
  basicstyle=\ttfamily\footnotesize,
  breakatwhitespace=false,         
  breaklines=true,                 
  captionpos=b,                    
  keepspaces=true,                 
  numbers=left,                    
  numbersep=5pt,                  
  showspaces=false,                
  showstringspaces=false,
  showtabs=false,                  
  tabsize=2
}

\lstset{style=mystyle}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle
\tableofcontents
\begin{center}
    This work is licensed under a \href{http://creativecommons.org/licenses/by-nc/4.0/}{Creative Commons Attribution-NonCommercial 4.0 International License}.

\end{center}

\newpage

\chapter{Limits and Continuity}

\section{Analytical Evaluation of Limits}
\begin{multicols}{2}
\begin{center}

$$\lim_{x\to c} k = k$$

$$\lim_{x\to c} x = c$$

$$\lim_{x\to c} [k*f(x)] = k * \lim_{x \to c} f(x)$$

$$\lim_{x\to c} [f(x) \pm g(x)] = \lim_{x\to c} f(x) \pm \lim_{x\to c} g(x)$$
\end{center}
\end{multicols}

\section{Indeterminate Form}

Indeterminate Form means that the evaluation of the limit has failed. It happens when a limit evaluation returns one of the following values: $\frac{0}{0}, \frac{0}{\pm \infty}, \frac{\pm \infty}{0}, \frac{\pm \infty}{\pm \infty}$

\section{Special Limits}
\begin{multicols}{3}
\begin{center}

$$\lim_{x\to 0} \frac{\sin{x}}{x} = 1$$

$$\lim_{x\to 0} \frac{x}{\sin{x}} = 1$$

$$\lim_{x\to 0} \frac{1-\cos{x}}{x} = 0$$

$$\lim_{x\to 0} (1+x)^{\frac{1}{x}} = e$$

$$\lim_{x\to 0} \cos{\frac{1}{x}} = 1$$


\end{center}
\end{multicols}
\newpage

\section{Discussing Continuity}

\begin{itemize}
    \item A \emph{continuous function} has $\lim_{x\to c^-} f(x) = \lim_{x\to c^+} f(x) = f(c)$ for all values $c$ on the domain.
    \item A \emph{removable discontinuity} has $\lim_{x\to c^-} f(x) = \lim_{x\to c^+} f(x) \neq f(c)$ for value $c$ on the domain.
    \item A \emph{non-removable discontinuity} has $\lim_{x\to c^-} f(x) \neq \lim_{x\to c^+} f(x)$ for value $c$ on the domain.
    \item An \emph{everywhere continuous} function is \emph{continuous}  the entire domain ($-\infty, \infty$)


\end{itemize}

\section{Intermediate Value Theorem}
If
\begin{itemize}
    \item a function is continuous\footnote{Absolutely have to demonstrate this if used on AP test} on a closed interval f(a) and f(b)
    \item L is between $f(a)$ and $f(b)$
\end{itemize}
Then there exists at lease one number $c$ on the open interval $(a,b)$ such that $f(c) = L$

\section{Asymptotes and End Behavior}
\subsection{Vertical Asymptotes}
A vertical Asymptote occurs when both are true (note that they can both be headed towards the same signed infinity):
\begin{multicols}{2}
\begin{center}
$$\lim_{x\to c^+} f(x) = \pm \infty$$

$$\lim_{x\to c^-} f(x) = \mp \infty$$
\end{center}
\end{multicols}


\subsection{Horizontal Asymptotes and End Behavior}

\emph{End behavior} describes the limit as a function goes to $x=\infty$ or $x=-\infty$.

In many functions, including \textbf{all polynomials}, it will go to $\pm \infty$. However, in some functions, it will go towards $y=k$. In this case, the function has a \emph{horizontal asymptote}. If a function has different limits as $x\to-\infty$ and $x\to\infty$, there may be multiple horizontal asymptotes.

The $\frac{1}{x}$ family of functions are the most common that have horizontal asymptotes.

To evaluate $\lim_{x\to\pm\infty} f(x)$, only compare the highest degree of the numerator and denominator of the function. For example, $\frac{3x^2}{x}$ does not have a horizontal asymptote, since the numerator is of a higher degree than the denominator. On the other hand, $\frac{3x^2+2x}{5x^2}$ has a horizontal asymptote of $y=\frac{3}{5}$, since $\frac{3*(\infty)^2}{5*(\infty)^2} = \frac{3}{5}$. The infinities can be ignored, since $\infty^2$ acts as a constant.

\chapter{Derivatives}
\section{Derivatives Basics}
\subsection{Characteristics of Derivatives}
\begin{itemize}
    \item Differentiability implies continuity
    \item $\lim_{x\to c^-} f(x) = \lim_{x\to c^+} f(x)$
    \item Cusps and Corners are not differentiable, because $\lim_{x\to c^-} \frac{f(x)-f(c)}{x-c} \neq \lim_{x\to c^+}\frac{f(x)-f(c)}{x-c}$
\end{itemize}
\subsection{Rates of Change}
$$\frac{f(b)-f(a)}{b-a}$$
This is used for the average rate of change between a and b. The derivative is just the rate of change. However, much of what we do during calculus is finding the instantaneous rate of change.
\subsection{Derivative Notation}
$$f'(x), \frac{dy}{dx}, y', \frac{d}{dx}[f(x)]$$
\subsection{Tangent Line Derivatives}
$$\lim_{\Delta x \to 0} \frac{\Delta y}{\Delta x} = \lim_{\Delta x \to 0} \frac{f(c+\Delta x)-f(c)}{\Delta x} = m$$

\newpage
\section{Differentiation Rules}
\begin{multicols}{3}
\begin{center}
    $\frac{d}{dx} [c]=0$

$\frac{d}{dx} [cu] = cu'$

$\frac{d}{dx}(x^n) = n*x^{n-1}$

$\frac{d}{dx} [\frac{u}{v}] = \frac{vu'-uv'}{v^2}; v \neq 0$

$\frac{d}{dx} |u| = \frac{u}{|u|}*u'$

$\frac{d}{dx} [\int_{a}^{x} f(t)dt] = f(x)$

$\frac{d}{dx} [\int_{a}^{u(x)} f(t)dt] = f(u(x)) * u'$

$\frac{d}{dx} [\ln{u}] = \frac{u'}{u}; u > 0$

$\frac{d}{dx} [\ln{u}] = \frac{u'}{u}; u \neq 0$

$\frac{d}{dx} [\log_a (u)] = \frac{u'}{ln(a) * u}; a > 0$

$\frac{d}{dx}[\sin{u}] = \cos{u} * u'$

$\frac{d}{dx}[\cos{u}] = -(\sin{u}) * u'$

$\frac{d}{dx}[\tan{u}] = -(\sec^2{u}) * u'$

$\frac{d}{dx}[\cot{u}] = -(\csc^2{u}) * u'$

$\frac{d}{dx}[\sec{u}] = [\sec{u}\tan{u}] * u'$

$\frac{d}{dx}[\csc{u}] = [\csc{u}\cot{u}] * u'$

$\frac{d}{dx}[u \pm v] = u' \pm v'$

$\frac{d}{dx}[u*v] = u*v'+v*u'$

$\frac{d}{dx}[\frac{f(x)}{g(x)}] = \frac{f'(x)*g(x)-f(x)*g'(x)}{[g(x)]^2}; g(x) \neq 0$

$\frac{d}{dx}[u^n] = n*u^{n-1}*u'$

$\frac{d}{dx}[f(g(x))] = f'(g(x))*g'(x)$

$\frac{d}{dx}[f(u)] = f'(u)*u' = \frac{d[f(u)]}{du} * \frac{du}{dx}$

$\frac{d}{dx}[e^u] = e^u*u'$

$\frac{d}{dx}[a^u] = ln(a) * a^u*u'$

$\frac{d}{dx}[\arcsin{u}] = \frac{u'}{\sqrt{1-u^2}}$

$\frac{d}{dx}[\arccos{u}] = \frac{-u'}{\sqrt{1-u^2}}$

$\frac{d}{dx}[\arctan{u}] = \frac{u'}{1+u^2}$

$\frac{d}{dx}[\arccot{u}] = \frac{-u'}{1+u^2}$

$\frac{d}{dx}[\arcsec{u}] = \frac{u'}{|u|\sqrt{u^2-1}}$

$\frac{d}{dx}[\arccsc{u}] = \frac{-u'}{|u|\sqrt{u^2-1}}$
\end{center}

\end{multicols}

\subsection{Bases other than e}
\subsubsection{Important Properties}

$$y=b^x = \log_b y = x $$
$$ b^{\log_b x} = x : b > 0 \And{} b \neq 1$$
$$\log_b b^x = x: b > 0 \And{} b \neq 1$$
$$\frac{d}{dx} \log_a x = \frac{1}{\ln{(a)}x}$$
\section{Inverse Functions in General}
Let f be a function differentiable on an interval, I. If f has an inverse function, $g(x)=f^{-1}(x)$, then $g(x)$ is differentiable at any x for which $f'(g(x)) \neq 0$. Moreover, at point $x=c$

$$g'(c) = \frac{1}{f'(g(c))}$$



\section{Theorems and Extrema}
\subsection{Extreme Value Theorem}
If the function is continuous over a closed interval, then f has a maximum and minimum over the interval.

\subsection{Relative/Local Extrema}
A relative extrema a place that is higher than the points immediately to the left and right. It CANNOT be an endpoint.

\subsection{Critical Point}
A critical point is where $f'(c) = 0$ or $f'(c) = DNE$

A critical point can be the site of a relative extrema.

\subsection{Mean Value Theorem}
Must be:
\begin{itemize}
    \item Continuous over $[a.b]$
    \item Differentiable over $(a,b)$
\end{itemize}

There is a point between a and b where the actual slope is equal to average slope
$$f'(c) = \frac{f(b)-f(a)}{b-a}$$

\subsection{Inverse}

The derivative of the inverse is the reciprocal of the original's derivative
\section{Odd Vs Even}
Even is symmetric with y axis

$$f(-c) = f(c)$$

Odd is symmetric around origin (such as sin)

$$\int_{-a}^a f(x)dx = 0$$

\chapter{Integrals}
\section{Integration Rules}
\begin{multicols}{2}
\begin{center}
    $\int\frac{d}{dx}[f(u)]dx = f(u)+C$

$\int kf(u)du = k\int f(u)du$

$\int x^n = \frac{x^{n+1}}{n+1}$

$\int(f(u) \pm g(u))du = \int f(u)du \pm \int(g(u)du)$

$\int e^udu = e^u+C$

$\int a^udu = \frac{a^u}{\ln{a}}$

$\int \sin{u}du = -\cos{u}+C$

$\int \cos{u} du = \sin{u}+C$

$\int \tan{u}du = -\ln{|\cos{u}|}+C$

$\int \cot{u}du = \ln{|\sin{u}}+C$

$\int \sec{u}du = \ln{|\sec{u}+\tan{u}|} + C$

$\int \csc{u}du = -\ln{|\csc{u} + \cot{u}|} + C$

$\int \sec^2{u}du = \tan{u}+C$

$\int \csc^2{u}du = -\cot{u}+C$

$\int \sec^2{u}du = \tan{u}+C$

$\int \csc^2{u}du = -\cot{u}+C$

$\int \sec{u} \tan{u}du = \sec{u}+C$

$\int du = u + C$

$\int{u^n}du = \frac{u^{n+1}}{n+1}+C; n\neq-1$

$\int{\frac{du}{u}}du = \ln{|u|}+C$

$\int{\frac{du}{\sqrt{a^2-u^2}}} = \arcsin{(\frac{u}{a})}+C$

$\int{\frac{1}{u} \frac{du}{\sqrt{u^2-a^2}}} = \frac{1}{a} \arcsec{(\frac{|u|}{a})}+C$

$\int{\frac{du}{a^2+u^2}} = \frac{1}{a} \arctan{(\frac{|u|}{a})}+C$

\end{center}{}


\end{multicols}
\section{Tip for Integrating Rational Functions}
If the degree of the numerator is $\geq$ that of the denominator, divide using long division of the polynomials. Then integrate the sum of the quotient and the remaining rational function separately.
\section{Method of u Substitution}
Select $u=g(x)$, typically he inner function in a composition. Compute $du=g'(x)dx$. If needed, manipulate the integrand by multiplying and dividing by a constant to obtain $g'(x)dx$. Re-write:

$\int{f(g(x))*g'(x)dx}=\int{f(u)du}$

Integrate w/r/t/y and then substitute $u=g(x)$ back into the antiderivative.
\section{Theorems}
\subsection{Mean Value Theorem for Integrals}

It guarantees that a rectangle can be drawn with the actual area. It must be continuous on [a.b]

$$\int_a^b f(x)dx = f(c)(b-a)$$

\subsection{Actual Value of a Function}
Requires that f is integratable on [a,b

$$\frac{a}{b-a} \int_a^b f(x)dx = f(c)$$


    Example: Average value of $3x^2-2x$ on [1,4]

$$\frac{1}{4-1} \int_1^4 (3x^2-2x)dx = f(c)$$

$$\frac{1}{3}(x^3-x^2)|_1^4$$

$$\frac{1}{3}[(4^3-4^2)-(1^3-1^2)]$$

$$\frac{1}{3}48 = 16$$


\chapter{Types and Characteristics of Graphs}
\section{Implicit vs Explicit}
\subsection{Implicit}
Example: $x^2+y^2=1$

Implicit methods do not directly relate x to y, and one value of x may have multiple values of y. A good example is $x^2+y^2=1$, for which a given value of $x$ (such as 0), may have multiple $y$ values (such as $(0,1)$ and $(0,-1)$).

\subsection{Explicit}
Example: $y=\frac{1}{2}(1-x^2)$

Explicit methods have an input and output. Every input has only one output. It is identified in a $y=?$ format. It can be solved normally, and is the more simplistic classification.

\chapter{Variation}
\section{Direct Variation}
$$y=kx$$
\section{Inverse Variation}
$$y = \frac{k}{x}$$
\section{General Format}
$$y=Ce^{kt}$$

\section{Newton's Law of Cooling}
The rate of change in temperature of an object is proportional to the difference between the object's temperature and the temperature of the surrounding medium.


\chapter{Area}
\section{Sigma}
\subsection{Properties}
$$\sum_{i=1}^{n} k*a_i = k\sum_{i=1}^{n}a_i$$

$$\sum_{i=1}^{n} (a_i \pm b_i) = \sum_{i=1}^{n}a_i+\sum_{i=1}^{n}b_i$$
\subsection{Summation Formulas}
$$\sum_{i=1}^{n} c = c*n_1$$
$$\sum_{i=1}^{n} i = \frac{n(n+1)}{2}$$
$$\sum_{i=1}^{n} i^2 = \frac{n(n+1)(2n+1)}{6}$$
$$\sum_{i=1}^{n} i^3 = \frac{n^2(n+1)^2}{4}$$


\section{Riemann Sums}
\subsection{Vocabulary}
\begin{itemize}
    \item Norm: Largest sub-interval of a partition - $||\Delta||$
    \item Regular Partition: All sub-intervals are equal - $||\Delta|| = \Delta x$
    \item If $||\Delta||$ is 0 then there are infinite intervals
\end{itemize}

\subsection{Trapezoid Sums}

Use trapezoids with $b_1 = f(a)$, $b_2 = f(b)$, $h = b-a$

Area of a trapezoid: $A = \frac{b_1 + b_2}{2} *h$


$$\lim_{||\Delta|| \to 0} \sum_{i = 1}^{n} f(C_i) \Delta x_i = \int_a^b f(x)dx$$

\section{Area Between Two Curves}
$$\int_a^b [f(x)-g(x)]dx$$

To get the area between two curves, take the integral of the upper one minus the lower one.

If the curves cross, you may need to take the integral of each area separately


\chapter{Fundamental Theorems of Calculus}
\section{First Fundamental Theorem of Calculus}
If f is continuous at every point on [a,b] and if F is any antiderivative on [a,b] then:
$$\int_a^b f(x)dx = F(b)-F(a)$$

\section{Second Fundamental Theorem of Calculus}

$$\frac{d}{dx} \int_a^x f(t)dt = f(x)$$


\chapter{Disks and Washer Volumes}
\section{Disks}
\subsection{Characteristics}
\begin{itemize}
    \item Only works for solid shapes
    \item Perpendicular Cross Section
    \item Radius = $f(x)$
    \item Thickness of disk = $\Delta $
\end{itemize}
\subsection{Formula}

$$v = \int_0^x \pi[R(x)]^2dx$$

\section{Washer}
Use when a solid has a solid core
$$V = Cyl_{big} - Cyl_{sma}$$
$$\pi R^2 \Delta x - \pi r^2 \Delta x$$
$$\pi \Delta x(R^2-r^2)$$
$$V = \int_a^b \pi([R(x)]^2-[r(x)]^2)dx$$

\section{Non-circular shapes}
$\int_a^b A(x)$ where $A(x)$ is the height of the object perpendicular to the line containing a and b

\chapter{L'Hopital's Rule}
\section{Criteria}
\begin{itemize}
    \item $\lim_{x\to\infty}$ means the end behavior in the positive direction. If it equals a constant, it represents an asymptote.
    \item $\lim_{x\to 0}$ is the value when $x$ approaches 0 from both sides. It exists when the limits from the right and left are the same
\end{itemize}
\section{Definition}

Let f and g be functions that are differentiable on the open interval (a,b) containing c, except possibly at c itself.

If $\lim_{x\to c} \frac{f(x)}{g(x)}$ is indeterminant, then:

$$ \lim_{x \to c} \frac{f(x)}{g(x)} =\lim_{x \to c} \frac{f'(x)}{g'(x)} $$

\section{Things to Remember}
$$\lim_{x\to 0} \frac{\sin{x}}{x} = 1$$

\begin{itemize}
    \item L'Hopital only works for fractions
\end{itemize}


\chapter{Geometry}
\section{2D Shapes}
\subsection{Square}
\begin{itemize}
    \item $s$ = side length
    \item $A$ = area
    \item $p$ = perimeter
\end{itemize}
\subsubsection{Area}
$$A = s^2$$
\subsubsection{Perimeter}
$$p = 4s$$
\subsection{Circle}
\begin{itemize}
    \item $r$ = radius
    \item $d$ = diameter
    \item $c$ = circumference
    \item $A$ = area
\end{itemize}
\subsubsection{Area}
$$A = \pi r^2$$
\subsubsection{Circumference}
$$c = 2\pi r$$
$$c = \pi d$$

\subsection{Triangle}
\begin{itemize}
    \item $h$ = height
    \item $B$ = base
    \item $A$ = area
    \item $p$ = perimeter
    \item $a$ = Side A
    \item $b$ = Side B
    \item $c$ = Side C
\end{itemize}

\includegraphics{Screenshot_20200509_180142.png}

\subsubsection{Area}
$$A = \frac{hB}{2}$$

\subsubsection{Height}
$$h = 2*\frac{A}{B}$$

\subsubsection{Side A}
$$a = 2 * \frac{A}{B\sin{\gamma}}$$

\subsubsection{Side C}

$$c=p-b-a$$

\section{3D}
\subsection{Rectangular Prism}
\begin{itemize}
    \item $l$ = length
    \item $w$ = width
    \item $h$ = height
\end{itemize}

\subsubsection{Surface Area}

$$SA = 2lw+2lh+2wh$$

\subsubsection{Volume}

$$V = lwh$$

\subsection{Sphere}
\begin{itemize}
    \item $r$ = radius
\end{itemize}

\subsubsection{Surface Area}

$$SA = 4 \pi r^2$$

\subsubsection{Volume}

$$V = \frac{4}{3} \pi r^3$$

\subsection{Cylinder}

\begin{itemize}
    \item $r$ = radius
    \item $h$ = height
\end{itemize}

\subsubsection{Surface Area}

$$SA = 2 \pi r^2$$

\subsubsection{Volume}

$$V = \pi r^2h$$

\newpage

\subsection{Cone}
\begin{itemize}
    \item $r$ = radius
    \item $h$ = height
\end{itemize}

\subsubsection{Slant Height}

$$l = \sqrt{r^2+h^2}$$

\subsubsection{Surface Area}

$$SA = \pi r(r+\sqrt{h^2+r^2})$$

\subsubsection{Volume}

$$V = \pi r^2 \frac{h}{3}$$

\end{document}

